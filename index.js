const Koa = require('koa')
const Router = require('koa-router')
const logger = require('koa-logger')
const chalk = require('chalk')
const R = require('ramda')
const faker = require('faker')

const main = async () => {
  const app = new Koa()
  const router = new Router()

  // const client = await data(appID, accessKey)
  //
  // // uplink event cache
  // const eventCache = {}
  // client.on('uplink', function(devId, message) {
  //   // init the slot, if it's new
  //   if (typeof eventCache[devId] === 'undefined') {
  //     eventCache[devId] = []
  //   }
  //
  //   // insert latest message
  //   eventCache[devId].push(message)
  // })

  const students = R.times(
    (n) => ({
      name: faker.name.findName(),
      email: faker.internet.email(),
      id: faker.random.uuid(),
      version: 1,
    }),
    500,
  )
  // console.log(students)

  // routes
  router.get('/students', async (ctx) => {
    ctx.body = JSON.stringify({
      time: Date.now(),
      students: students,
    })

    ctx.status = 200
  })

  // router.get('/events/all', async (ctx) => {
  //   logAction(`Sending full event list`, eventCache)
  //
  //   ctx.body = JSON.stringify(eventCache)
  //
  //   ctx.status = 200
  // })
  //
  // router.get('/events/latest/:device', async (ctx) => {
  //   logAction(`Sending latest event from ${ctx.params.device}`)
  //
  //   const deviceEvents = eventCache[ctx.params.device]
  //   const latestEvent = deviceEvents[deviceEvents.length - 1]
  //
  //   console.log(latestEvent)
  //
  //   ctx.body = JSON.stringify(lastestEvent)
  //   ctx.status = 200
  // })

  // request handler
  app.use(router.routes())
  app.use(router.allowedMethods())

  // start the server
  const listenPort = process.env.PORT || 4002
  logAction(`Listening on port *:${listenPort}`)
  app.listen(listenPort)
}

function logAction(...props) {
  return console.log(chalk.blue('==>', ...props))
}

main()
